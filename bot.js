'use strict';

var Botson = require('./lib/botson');

//var token = process.env.BOT_API_KEY;
var dbPath = process.env.BOT_DB_PATH;
var name = process.env.BOT_NAME;

var token_apendo = process.env.BOT_API_KEY_APENDO;
var token_hakandev = process.env.BOT_API_KEY_HAKANDEV;

var botson_apendo = new Botson({
    token: token_apendo,
    dbPath : dbPath,
    name: name
})

botson_apendo.run();

var botson_hakandev = new Botson({
    token: token_hakandev,
    dbPath : dbPath,
    name: name
})

botson_hakandev.run();